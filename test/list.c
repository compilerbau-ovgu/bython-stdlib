#include <stdio.h>
#include <stdlib.h>

#include "bython-stdlib.h"

static int list_string_test()
{
	puts("----- string tests -----");
	List* list = list_string_init();

	for (int i = 0; i < 19; ++i) {
		char* str = malloc(12);
		sprintf(str, "%d", i);
		list = list_push_back(list, str);
	}
	list_print(list);
	puts("");

	return EXIT_SUCCESS;
}

static int list_int_test()
{
	puts("----- int tests -----");
	List* list = list_int_init();

	for (int_least32_t i = 0; i < 19; ++i) {
		list = list_push_back(list, i);
	}
	list_print(list);
	puts("");

	for (int_least32_t i = list->size - 1; i >= 0; --i) {
		list = list_set(list, list->size - 1 - i, i);
	}
	list_print(list);
	puts("");

	return EXIT_SUCCESS;
}

static int llist_test() {
	puts("Puting");
	llist list = NULL;
	for(int i = 0; i < 20; ++i){
		list = llist_push_alloc(&i, sizeof(i), (void*) list);
		printf("%d\n", *((int*) llist_peek(list)));
	}
	printf("%d\n", sizeof(void*));
	puts("Poping");
	do {
		printf("%d\n", *((int*) llist_peek(list)));
		list = llist_pop(list);
	}while(list!= NULL);


	return EXIT_SUCCESS;
}

int main(int argc, char** argv)
{
	(void) argc;
	(void) argv;

	int err = list_int_test() + list_string_test() + llist_test();
	internal_list_free_all();

	return err;
}
