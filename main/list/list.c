#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static List* collector = NULL;

static void internal_list_push_back(List* list, void* value);
static void internal_list_check_size(List* list, int32_t size);

static void internal_list_free(List* list);

static List* internal_list_init(TYPE type,
								int32_t bytes,
								int32_t data_alloc_size);

static List* internal_list_copy(List* in);

static void internal_register_list_in_collector(List* register_me);

static void internal_list_set(List* list, int32_t index, void* value);

static List* internal_list_init(TYPE type,
								int32_t bytes,
								int32_t custom_alloc_size)
{
	List* l = malloc(sizeof(List));

	l->data_size = bytes;
	l->capacity = INITIAL_CAPACITY;
	l->data_type = type;

	if (custom_alloc_size == 0) {
		l->size = 0;
		l->data = malloc(l->data_size * INITIAL_CAPACITY);
	}
	else {
		l->size = custom_alloc_size;
		l->data = malloc(custom_alloc_size * l->data_size * INITIAL_CAPACITY);
	}
	return l;
}

static List* internal_list_copy(List* in)
{
	List* new_list = internal_list_init(in->data_type, in->data_size, in->size);
	memcpy(new_list->data, in->data, in->size * in->data_size);

	return new_list;
}

static void internal_list_set(List* list, int32_t index, void* value)
{
	switch (list->data_type) {
		case Int: {
			int32_t* data = (int32_t*) list->data;
			int32_t val = (int32_t) value;
			data[index] = val;
			break;
			// memcpy(data + index, val, list->data_size);
		}
		case String: {
			char** data = (char**) list->data;
			char* val = (char*) value;
			data[index] = val;
			break;
			// memcpy(data + index, val, list->data_size);
		}

		// TODO: Implement compatibility with float
		case Float: {
			float* data = (float*) list->data;
			// float* val = (float) value;
			memcpy(data + index, value, list->data_size);
			break;
		}
	}

	// memcpy(list->data + index * list->data_size, value, list->data_size);
}

// Make independent from all other functions to avoid conflicts
static void internal_register_list_in_collector(List* register_me)
{
	if (register_me == NULL) {
		printf("ERROR: %" PRIdLEAST32 "th allocation wants to register 0x0\n",
			   collector->size);
	}

	// more elements would outgrow array, reallocate
	if (collector->size + 1 >= collector->capacity) {
		const int32_t new_size =
			(int32_t)(SIZE_GROWTH_FACTOR * collector->size);
		/*printf("Reallocating collector to %" PRIiLEAST32 " elements\n",
			   new_size);*/
		collector->data =
			realloc(collector->data, new_size * collector->data_size);
		collector->capacity = new_size;
	}

	((List**) collector->data)[collector->size++] = register_me;
}

void internal_list_push_back(List* list, void* value)
{
	list->size++;

	internal_list_check_size(list, list->size);
	internal_list_set(list, list->size - 1, value);
}

void internal_list_free_all(void)
{
	if (collector == NULL || collector->data == NULL) {
		return;
	}

	const int32_t size = list_size(collector);
	for (int32_t i = 0; i < size; ++i) {
		List* ilist = ((List**) collector->data)[i];
		internal_list_free(ilist);
	}

	internal_list_free(collector);
	collector = NULL;
}

List* list_init(TYPE type, int32_t bytes)
{
	// Init collector for all lists
	if (collector == NULL) {
		// Use String, all pointers should have same width
		collector = internal_list_init(String, sizeof(List**), 0);
	}

	List* l = internal_list_init(type, bytes, 0);
	internal_register_list_in_collector(l);

	return l;
}

List* list_int_init(void)
{
	return list_init(Int, sizeof(int32_t));
}

List* list_float_init(void)
{
	return list_init(Float, sizeof(float));
}

List* list_string_init(void)
{
	return list_init(String, sizeof(char*));
}

List* list_pop_front(List* list)
{
	List* new_list = internal_list_copy(list);
	new_list->size--;

	switch (list->data_type) {
		case Int: {
			int32_t* data = list->data;
			memmove(
				new_list->data, data + 1, new_list->size * new_list->data_size);
		}
		case String: {
			char* data = list->data;
			memmove(
				new_list->data, data + 1, new_list->size * new_list->data_size);
		}
		case Float: {
			float* data = list->data;
			memmove(
				new_list->data, data + 1, new_list->size * new_list->data_size);
		}
	}

	internal_register_list_in_collector(new_list);
	return new_list;
}

List* list_push_back(List* list, void* value)
{
	List* new_list = internal_list_copy(list);
	internal_list_push_back(new_list, value);
	internal_register_list_in_collector(new_list);
	return new_list;
}

void* list_get(List* list, int32_t index)
{
	// TODO: Isn't pointer arithmetic on void types UB?
	if (index >= 0 && index < list->size) {
		return list->data + index * list->data_size;
	}
	return NULL;
}

int32_t list_get_int(List* list, int32_t index)
{
	return ((int32_t*) list->data)[index];
}

int32_t list_size(List* list)
{
	return list->size;
}

List* list_set(List* list, int32_t index, void* value)
{
	if (index >= 0 && index < list->size) {
		List* new_list = internal_list_copy(list);
		internal_list_set(new_list, index, value);

		internal_register_list_in_collector(new_list);
		return new_list;
	}

	return NULL;
}

List* list_merge(List* list1, List* list2)
{
	if (list1->data_size != list2->data_size ||
		list1->data_type != list2->data_type) {
		return NULL;
	}

	List* list = internal_list_copy(list1);
	for (int32_t i = 0; i < list2->size; ++i) {
		internal_list_push_back(list, list_get_int(list2, i));
	}

	internal_register_list_in_collector(list);
	return list;
}

void list_print(List* list)
{
	if (list->size > 0) {
		if (list->data_type == Int) {
			printf("[ %d", *((int32_t*) list_get(list, 0)));
			for (int i = 1; i < list->size; ++i) {
				printf(", %d", *((int32_t*) list_get(list, i)));
			}
			printf("]");
		}
		if (list->data_type == Float) {
			printf("[ %f", *((float*) list_get(list, 0)));
			for (int i = 1; i < list->size; ++i) {
				printf(", %f", *((float*) list_get(list, i)));
			}
			printf("]");
		}
		if (list->data_type == String) {
			printf("[ %s", *((char**) list_get(list, 0)));
			for (int i = 1; i < list->size; ++i) {
				printf(", %s", *((char**) list_get(list, i)));
			}
			printf("]");
		}
	}
	else {
		printf("[]");
	}
}

void internal_list_check_size(List* list, int32_t size)
{
	if (list->capacity < size) {
		int32_t new_size = (int32_t)(size * SIZE_GROWTH_FACTOR);
		list->data = realloc(list->data, new_size * list->data_size);
		list->capacity = new_size;
	}
}

void internal_list_free(List* list)
{
	free(list->data);
	free(list);
}


// Linked List

llist llist_push_alloc(void* data, size_t memory, llist next) {
	void* data_alloc = malloc(memory);
	memcpy(data_alloc, data, memory);
	return llist_push(data_alloc, next);
}

llist llist_push(void* data, llist next) {
	llist new = (llist) malloc(sizeof(struct node));
	new->data = data;
	new->next = next;
	return new;
}

void* llist_peek(llist list) {
	return list->data;
}

bool llist_has_next(llist list) {
	return list->next != NULL;
}

llist llist_pop(llist list) {
	llist next = list->next;
	free(list->data);
	free(list);
	return next;
}


llist list_init_main(int32_t argc, char** argv){
	llist list = NULL;
  for(int i = argc-1; i >= 0; --i){
      list = llist_push_alloc( argv[i], sizeof(char*), list);
  }
  return list;
}
