#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define INITIAL_CAPACITY 16
#define SIZE_GROWTH_FACTOR 1.5

typedef enum { Int = 1, Float = 2, String = 3 } TYPE;

typedef struct {
	// amount of elements in the list
	int32_t size;

	// pointer to the dynamic data itself
	void* data;

	// maximum amount of elements that can be currently stored
	int32_t capacity;

	// type of the data contained
	TYPE data_type;

	// size of a single element stored by this array
	int32_t data_size;
} List;

// Functions to expose to Bython
List* list_int_init(void);
List* list_float_init(void);
List* list_string_init(void);
List* list_merge(List* list1, List* list2);
List* list_pop_front(List* list);

void list_print(List* list);

int_least32_t list_size(List* list);
List* list_push_back(List* list, void* value);

void* list_get(List* list, int_least32_t index);
int_least32_t list_get_int(List* list, int_least32_t index);

List* list_set(List* list, int_least32_t index, void* value);

// Expose for tests
// DO NOT EXPOSE TO BYTHON ITSELF!
void internal_list_free_all(void);


// Linked List

struct node{
	void* data;
	struct node * next;
};

typedef struct node* llist;

llist llist_push_alloc(void* data, size_t memory, llist next);
llist llist_push(void* data, llist next);
void* llist_peek(llist list);
llist llist_pop(llist list);
bool llist_has_next(llist list);



