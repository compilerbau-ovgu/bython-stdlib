#include "abs.h"

//
int_least32_t iabs(int_least32_t n)
{
	if (n < 0) {
		n *= -1;
	}
	return n;
}
