#pragma once

#include <inttypes.h>

int_least32_t iabs(int_least32_t);
