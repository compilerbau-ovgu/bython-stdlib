#include "print.h"

void print_int(int_least32_t i)
{
	printf("%" PRIdLEAST32, i);
}
//
void println_int(int_least32_t i)
{
	printf("%" PRIdLEAST32 "\n", i);
}

void println_str(const char* s)
{
	printf("%s\n", s);
}

void print_str(const char* s)
{
	printf("%s", s);
}

void println_float(float f)
{
	printf("%.6f\n", f);
}

void print_float(float f)
{
	printf("%.6f", f);
}