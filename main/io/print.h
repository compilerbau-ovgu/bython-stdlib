#pragma once

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

void print_int(int_least32_t);
void println_int(int_least32_t);
void println_str(const char*);
void print_str(const char*);
void println_float(float);
void print_float(float);